<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MovieTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_movies(): void
    {
        $user = User::factory()->create();

        $response = $this->get('/api/movies');

        $response->assertOk();
    }

    public function test_create_favorite_movie_without_metadata(): void
    {
        $user_id = 1;
        $response = $this->post('/api/movies/'.$user_id);

        $response->assertStatus(400);
    }

    public function show_favorite_movies(): void
    {
        $user_id = 1;

        $response = $this->get('/api/movies/1');

        $response->assertStatus(200);
    }
}