<h1>Movies App</h1>

<h2 style="color: red;">Disclaimer</h2>
<p>Essa é a minha primeira vez utilizando PHP e o Laravel framework. Acredito que não utilizei as melhores práticas possíveis tanto com os controllers, como o uso de alguns componentes e hooks do react, e também o deploy no vercel que na verdade não foi feito. Tentei focar em aprender sobre PHP e o Laravel para conseguir construir uma API funcional mesmo não conseguindo mockar e testar 100% todas as rotas, acredito que o projeto atende os requisitos básicos propostos com o tempo que tive para aprender as tecnologias</p>

# Setup

## Set up your environment variables

### use .env.example and create .env

### Steps to get your API key
<a href="https://rapidapi.com/apidojo/api/online-movie-database">IMDB API</a>
- Create an account/Log in
- Get your API key


### Install vendor packages
#### Make sure you have the rights to run the commands below
```properties
    composer install
```

### Install npm packages
```properties
    npm install
```

### Run containers
```properties
./vendor/bin/sail up -d
```

### Run migrations
```properties
./vendor/bin/sail artisan migrate
```
# build

### build project
```properties
npm run build
```

## Open app in [localhost](http://localhost:8000)