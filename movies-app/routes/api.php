<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MoviesController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth')->group(function () {
    Route::get('/movies', [MoviesController::class, 'index']);
    Route::post('/movies/{user_id}', [MoviesController::class, 'store']);
    Route::get('/movies/{user_id}', [MoviesController::class, 'show']);
    Route::delete('/movies/{user_id}/{id}', [MoviesController::class, 'destroy']);
});