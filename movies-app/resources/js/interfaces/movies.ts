export interface ImageInterface {
    height: number;
    id: string;
    url: string;
    width: string;
}

export interface IMovie {
    id: string;
    image?: ImageInterface;
    title: string;
    titleType: string;
    year: 1999
}

export interface IMovieResponse {
    id: number;
    user_id: number;
    metadata: IMovie
}