import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { FormEvent, useState } from 'react';
import { Head } from '@inertiajs/react';
import { PageProps } from '@/types';
import { api } from "../../services/api";
import { AxiosResponse } from 'axios';
import { IMovie, IMovieResponse } from '@/interfaces/movies';
import { CheckCircle } from '@phosphor-icons/react';

export default function ShowMovies({ auth }: PageProps<{}>) {

    const [title, setTitle] = useState<string>('');
    const [movies, setMovies] = useState<IMovie[]>([]);
    const [favoriteMovies, setFavoriteMovies] = useState<IMovieResponse[]>([])

    async function getFavoriteMovies() {
        try {
            const response: AxiosResponse = await api.get(`api/movies/${auth.user.id}`)
            setFavoriteMovies(response.data.data)
        } catch (error) {
            console.error(error);
        }
    }

    async function getMovies(e: FormEvent){
        e.preventDefault()
        if(!title) return;
        try{
            const encodedTitle = title.replaceAll(' ', '+')
            const response: AxiosResponse = await api.get(`api/movies/?title=${encodedTitle}`)
            setMovies(response.data.results)
            await getFavoriteMovies()
        } catch (error) {
            console.error(error);
        }
    }

    async function setFavoriteMovie(e: FormEvent, movieMetadata: IMovie){
        e.preventDefault()
        try{
            const response: AxiosResponse = await api.post(`api/movies/${auth.user.id}`, { metadata: movieMetadata })
            setFavoriteMovies(response.data.data)
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Profile</h2>}
        >
            <Head title="Movies" />
            <div className="w-full">
                <form
                    className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
                    onSubmit={(e) => getMovies(e)}
                >
                    <div className="mb-4">
                        <input
                            className="shadow text-center appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="title"
                            type="text"
                            placeholder="Search for a title"
                            value={title}
                            onChange={e => {
                                setTitle(e.target.value)
                            }}
                        />
                    </div>
                    <div className="flex items-center justify-center">
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                        >
                            Search
                        </button>
                    </div>
                </form>
                {movies.length ? (
                    <div
                        className="grid justify-items-center grid-cols-4 gap-8"
                    >
                        {movies.map(movie => {
                            return movie.image ?
                                <div key={movie.id}>
                                        <div
                                            className="flex justify-center flex-col p-5 rounded-md bg-slate-900 border-blue-500 border-2">
                                            <img src={movie.image.url} className="w-72 h-96"/>
                                            <span className="text-white text-xl text-right">{movie.year}</span>
                                            {favoriteMovies.find(favoriteMovie => favoriteMovie.metadata.id === movie.id) ? (
                                                <CheckCircle size={40} fill='green' className="mt-6"/>
                                            ) : (
                                                <button
                                                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 w-6/6 mt-6 rounded focus:outline-none focus:shadow-outline"
                                                    type="submit"
                                                    onClick={(e) => {setFavoriteMovie(e, movie)}}
                                                >
                                                    Make favorite
                                                </button>
                                            )}
                                        </div>
                                </div>
                            : null
                        })}
                    </div>
                ) :
                    <div className="flex items-center justify-center">
                        <h2>
                            No results
                        </h2>
                    </div>
                }
            </div>
        </AuthenticatedLayout>
    );
}
