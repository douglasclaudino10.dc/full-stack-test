import { FormEvent, useEffect, useState} from "react";
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Link, Head } from '@inertiajs/react';
import { PageProps } from '@/types';
import { AxiosResponse } from "axios";
import { api } from "@/services/api";
import { IMovieResponse } from "@/interfaces/movies";

export default function Dashboard({ auth }: PageProps) {

    const [favoriteMovies, setFavoriteMovies] = useState<IMovieResponse[]>([])
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function getMovies() {
            try {
                setLoading(true)
                const response: AxiosResponse = await api.get(`api/movies/${auth.user.id}`)
                setFavoriteMovies(response.data.data)
            } catch (error) {
                console.error(error);
            } finally {
                setLoading(false)
            }
        }
        getMovies();
    }, [])

    async function removeMovieFromFavorites(e: FormEvent, movieId: number){
        e.preventDefault()
        try{
            const response: AxiosResponse = await api.delete(`api/movies/${auth.user.id}/${movieId}`)
            setFavoriteMovies(response.data.data)
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 text-center">
                    <Link
                        href={route('movies')}
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 w-3/6 rounded focus:outline-none focus:shadow-outline"
                    >
                        Add favorite Movies
                    </Link>
                </div>
            </div>
            <div id="scroll-container" className="grid justify-items-center grid-cols-4 gap-8">
                {favoriteMovies.map(movie => {
                    return (
                        <div
                            key={movie.id}
                            className="flex justify-items-center flex-col p-5 rounded-md bg-slate-900 border-blue-500 border-2"
                        >
                            <img src={movie.metadata?.image?.url} className="w-72 h-96"/>
                            <span className="text-white text-xl text-right">{movie.metadata.year}</span>
                            <button type="submit" onClick={(e) => {removeMovieFromFavorites(e, movie.id)}}
                                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 w-6/6 mt-6 rounded focus:outline-none focus:shadow-outline"
                        >
                            Remove from favorite movies
                        </button>
                        </div>
                    )
                })}
            </div>
        </AuthenticatedLayout>
    );
}
