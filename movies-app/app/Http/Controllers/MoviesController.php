<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\MovieResource;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;

class MoviesController extends Controller
{
    public function index(Request $request)
    {
        $api_key = env('RAPID_API_KEY');
        $title = urlencode($request->input('title'));
        $page = urlencode($request->input('page'));

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', "https://online-movie-database.p.rapidapi.com/title/v2/find?title={$title}&titleType=movie&limit=10&paginationKey={$page}", [
            'headers' => [
                'X-RapidAPI-Host' => 'online-movie-database.p.rapidapi.com',
                'X-RapidAPI-Key' => $api_key,
            ],
        ]);

        return $response->getBody();
    }

    public function store($user_id, Request $request)
    {
        $jsonData = $request->json()->all();

        if (!isset($jsonData['metadata'])) {
            return response()->json(['error' => 'Metadata is missing'], 400);
        }
    
        $metadata = json_encode($jsonData['metadata'], true);

        if ($metadata === null) {
            return response()->json(['error' => 'Invalid metadata JSON'], 400);
        }

        $movie = Movie::create([
            'user_id' => $user_id,
            'metadata' => $metadata,
        ]);

        $movies = Movie::where('user_id', $user_id)->get();
        return MovieResource::collection($movies);
    }

    public function show($user_id, Request $request)
    {
        $perPage = urlencode($request->input('perPage'));
        $page = urlencode($request->input('page'));
        $movies = Movie::where('user_id', $user_id)->paginate($perPage, ['*'], 'page', $page);

        return MovieResource::collection($movies);
    }

    public function destroy($user_id, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();

        $movies = Movie::where('user_id', $user_id)->get();
        return MovieResource::collection($movies);
    }
}
